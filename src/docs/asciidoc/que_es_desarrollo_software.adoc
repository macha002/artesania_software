[.opaque]
== Que es desarrollo software?
image::screenshoot.png[background, size=cover]

[.opaque]
=== Producimos software?

[ditaa]
....
                +---------------------+            +----------------------+     +-----+
                |                     |            | computadoras         |     |     |
+--------+      | Ingenieros software |            | recibiendo datos,    |     | $$$ |
| tiempo +----> |                     +----------> | haciendo cálculos y  | <---+     |
+--------+      |                     |            | dando resultados     |     |     |
                +----------+----------+            +----------------------+     +-----+
                           ^
                           |
                           |
                         +-+-+
                         |   |
                         | $ |
                         |   |
                         +---+
....

image::data-center.jpg[background, size=cover]

[.opaque]
=== El tiempo es uno de los recursos más escasos...
[quote, Martin Sherlock, 'https://quoteinvestigator.com/2013/06/17/good-original/[Original]']
____
Your manuscript is both good and original; but the part that is good is not original, and the part that is original is not good.
____

image::reloj-de-arena.jpg[background, size=cover]

[.opaque]
=== Cómo producir software?
image::modern_times.jpg[background, size=cover]

[.opaque]
=== Modelo Waterfall
[ditaa]
....
+--------+     +----------+      +----------+      +------------+      +-------+      +----------------------+      +-----+
|        |     |          |      |          |      |            |      |       |      |                      |      |     |
| Inicio +---> | Análisis +----> | Diseño   +----> | Desarrollo +----> | Tests +----> | Puesta en producción +----> | Fin |
|        |     |          |      |          |      |            |      |       |      |                      |      |     |
+--------+     +----------+      +----------+      +------------+      +-------+      +----------------------+      +-----+

t --->
....

image::modern_times_serie.jpg[background, size=cover]